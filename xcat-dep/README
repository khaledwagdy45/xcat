This xcat-dep tarball contains open source pkgs that xCAT depends on, and is
provided for the convenience of the xCAT users.

Change logs
===========
The latest xcat-dep tar ball is xcat-dep-202001281100.tar.bz2

* Thu, 16 May 2019 04:16:27 -0400 GONG Jie <gongjie@linux.vnet.ibm.com>
  Update grub2-xcat from the latest source of RHEL 7
  - grub2-xcat-2.02-0.76.el7.1.snap201905160255.noarch.rpm

* Thu, 18 Apr 2019 16:40:28 +0800 GONG Jie <gongjie@linux.vnet.ibm.com>
  Build goconserver for SLES 12 x86-64
  - goconserver-0.3.2-snap201904180828.x86_64.rpm
  Build goconserver for SLES 12 ppc64le
  - goconserver-0.3.2-snap201904180606.ppc64le.rpm
  Build goconserver for RHEL 7 ppc64
  - goconserver-0.3.2-snap201904180332.ppc64.rpm

* Thu, 13 Dec 2018 10:51:52 +0800 GONG Jie <gongjie@linux.vnet.ibm.com>
  Remove everything but packages for RHEL 7, RHEL 8, and SLES 12
  Everything else are deprecated

* Thu, 29 Nov 2018 05:30:15 -0500 GONG Jie <gongjie@linux.vnet.ibm.com>
  Update grub2-xcat from the latest source of RHEL 7
  - grub2-xcat-2.02-0.76.el7.snap201811290530.noarch.rpm

* Mon, 19 Nov 2018 02:45:39 -0500 GONG Jie <gongjie@linux.vnet.ibm.com>
  Rebuild xCAT-genesis-base for both ppc64 and x86-64
  - xCAT-genesis-base-ppc64-2.14.5-snap201811160710.noarch.rpm
  - xCAT-genesis-base-x86_64-2.14.5-snap201811190037.noarch.rpm

* Fri, 09 Nov 2018 06:20:17 Er Tao Zhao <ertaozh@cn.ibm.com>
  Update goconserver-0.3.2-snap201811080416.x86_64.rpm for RHEL7 x86_64 system
  Update goconserver-0.3.2-snap201811080416.ppc64le.rpm for RHEL7 ppc64le system

* Thu, 01 Nov 2018 02:31:00 -0400 GONG Jie <gongjie@linux.vnet.ibm.com>
  Rebuild xCAT-genesis-base for both ppc64 and x86-64
  - xCAT-genesis-base-ppc64-2.14.5-snap201810310444.noarch.rpm
  - xCAT-genesis-base-x86_64-2.14.5-snap201810310444.noarch.rpm

* Mon, 20 Aug 2018 GONG Jie <gongjie@linux.vnet.ibm.com>
  Updated goconserver-0.3.1-snap201808210215.x86_64.rpm for RHEL7 x86_64 system
  Updated goconserver-0.3.1-snap201808202115.ppc64le.rpm for RHEL7 ppc64le system

* Wed, 4, Apr 2018 Victor Hu <vhu@us.ibm.com>
  Rebuild xCAT-genesis-base for ppc64 to fix GitHub Issue #5059
  - xCAT-genesis-base-ppc64-2.14-snap201804041553.noarch.rpm
    (a4c5becb1a6603662941270c5c2ee3fe)

* Fri, 31, Mar 2018 Long Cheng <chenglch@cn.ibm.com>
  Added goconserver-0.2.2-snap201803292146.x86_64.rpm for RHEL7 x86_64 system
  Added goconserver-0.2.2-snap201803292225.ppc64le.rpm for RHEL7 ppc64le system

* Thu, 29, Mar 2018 Er Tao Zhao <ertaozh@cn.ibm.com>
  Rebuild xCAT-genesis-base to include libnss_dns
  xCAT-genesis-base-ppc64-2.14-snap201803282253.noarch.rpm
  xCAT-genesis-base-x86_64-2.14-snap201803282249.noarch.rpm

* Fri, 2, Mar 2018 Long Cheng <chenglch@cn.ibm.com>
  Added goconserver-0.2.1-snap201803020115.x86_64.rpm for RHEL7 x86_64 system
  Added goconserver-0.2.1-snap201803020124.ppc64le.rpm for RHEL7 ppc64le system

* Wed, 17, Jan 2018 Er Tao Zhao <ertaozh@cn.ibm.com>
  New package xCAT-genesis-base-ppc64-2.13.10-snap201801170133.noarch.rpm 
  New package xCAT-genesis-base-x86_64-2.13.10-snap201801170129.noarch.rpm
  which is based on centos7.4 x86_64

* Tue, 14, Nov 2017 Er tao Zhao <ertaozh@cn.ibm.com>
  New package xCAT-genesis-base-ppc64-2.13.8-snap201711140035.noarch.rpm to
  replace xCAT-genesis-base-ppc64-2.13.6-snap201707280206.noarch.rpm
  - include all the drivers built in the host OS
  - no mlx4-en newer than 2.2-1 (Feb 2014) available on Fedora 26

* Thu, 28, July 2017 Er Tao Zhao <ertaozh@cn.ibm.com>
  New package xCAT-genesis-base-ppc64-2.13.6-snap201707280206.noarch.rpm to
  replace xCAT-genesis-base-ppc64-2.13.6-snap201707190550.noarch.rpm
  - based on Fedora 26 ppc64
  - i40e driver with version 1.6.27-k is included
  - no mlx4-en newer than 2.2-1 (Feb 2014) available on Fedora 26

*Fri, 16 Jun 2017 Long Cheng <chenglch@cn.ibm.com>
  Update packages to upgrade conserver to 8.2.1 version
  - Added conserver-xcat-8.2.1-1.<arch>.rpm
  - Removed conserver-xcat-8.1.16.<arch>.rpm

* Thu, 15, Jun 2017 Victor Hu <vhu@us.ibm.com>
  Updated ipmitool-xcat to 1.8.18 for RHEL7 and SLES12 OS

* Mon, 20 Feb 2017 Er Tao Zhao <ertaozh@cn.ibm.com>
  Update the packages to include perl library for ntp-wait command:
  - xCAT-genesis-base-ppc64-2.13.2-snap201702150049.noarch.rpm
  - xCAT-genesis-base-x86_64-2.13.2-snap201702200120.noarch.rpm

* Wed, 11 Jan 2017 Er Tao Zhao <ertaozh@cn.ibm.com>
  Update the following packages to include ntp-wait command:
  - xCAT-genesis-base-ppc64-2.13.1-snap201701110052.noarch.rpm
  - xCAT-genesis-base-x86_64-2.13.1-snap201701110114.noarch.rpm

* Mon, 05 Dec 2016 Victor Hu <vhu@us.ibm.com>
  Refreshed the xCAT-genesis-base rpms to have a more standard versioning:
    xCAT-genesis-base-ppc64-2.13.0-snap201612051541.noarch.rpm
    xCAT-genesis-base-x86_64-2.13.0-snap201612051408.noarch.rpm

* Wed, 23 Nov 2016 Er Tao Zhao<ertaozh@cn.ibm.com>
  Add xCAT-genesis-base-ppc64-2.12-snap201611230134.noarch.rpm which fix the
  NIC boot up issue

* Thu, 10 Nov 2016 Long Cheng <chenglch@cn.ibm.com>
  Update ipmitool-xcat to version 1.8.17-1 to solve console issues

* Tue, 25 Oct 2016 Er Tao Zhao <ertaozh@cn.ibm.com>
  Add xCAT-genesis-base-ppc64-2.12-snap201610250931.noarch.rpm which include
  the hard clock driver rtc-opal

* Mon, 26 Sep 2016 Patrick Lundgren <plundgr@us.ibm.com>
  Update ipmitool-xcat to version 1.8.17-0

* Fri, 12 Aug 2016 Long Cheng <chenglch@cn.ibm.com>
  Process HUP and TERM signal in ipmitool
  - Added ipmitool-xcat-1.8.15-3.ppc64.rpm for RHEL7 ppc64 systems
  - Added ipmitool-xcat-1.8.15-3.x86_64.rpm for RHEL7 x86_64 systems
  - Added ipmitool-xcat-1.8.15-3.x86_64.rpm for SLES11 x86_64 systems
  - Added ipmitool-xcat-1.8.15-3.ppc64le.rpm for RHEL7 ppc64le systems
  - Added ipmitool-xcat-1.8.15-3.ppc64le.rpm for SLES12 ppc64le systems
  - Removed ipmitool-xcat-1.8.15-3.ppc64.rpm for RHEL7 ppc64 systems
  - Removed ipmitool-xcat-1.8.15-3.x86_64.rpm for RHEL7 x86_64 systems
  - Removed ipmitool-xcat-1.8.15-3.x86_64.rpm for SLES11 x86_64 systems
  - Removed ipmitool-xcat-1.8.15-3.ppc64le.rpm for RHEL7 ppc64le systems
  - Removed ipmitool-xcat-1.8.15-3.ppc64le.rpm for SLES12 ppc64le systems

* Tue, 2 Aug 2016 Victor Hu <vhu@us.ibm.com>
  Replace ipmitool-xcat-1.8.15-2 for RHEL 6 x86_64 to fix libcrypto error

* Fri, 22 Jul 2016 Victor Hu <vhu@us.ibm.com>
  Refreshed the xCAT-genesis-base rpms to move the restart script, which is
  now in xCAT-genesis-scripts:
  - xCAT-genesis-base-ppc64-2.12-snap201607211138.noarch.rpm
  - xCAT-genesis-base-x86_64-2.12-snap201607212311.noarch.rpm

* Fri, 15 Jul 2016 Er Tao Zhao <ertaozh@cn.ibm.com>
  Add xCAT-genesis-base-ppc64-2.12-snap201607140518.noarch.rpm, updating
  mlx4-en driver to 3.2-1 for Mellanox Ethernet nics.  Remove
  xCAT-genesis-base-ppc64-2.12-snap201605051443.noarch.rpm

* Wed, 13 Jul 2016 Er Tao Zhao <ertaozh@cn.ibm.com>
  Rebuild ipmitool-xcat_1.8.15 with patch fixing the issue that console will
  hung for Firestone and Garrison node
  - Add ipmitool-xcat-1.8.15-2 for RHEL6 ppc64 and x86_64, RHELS7
    ppc64, ppc64le and x86_64, and remove ipmitool-xcat-1.8.15-1
  - Add ipmitool-xcat-1.8.15-2 for Sles 11 ppc64 and x86_64, Sles12 ppc64le
    and x86_64, and remove ipmitool-xcat-1.8.15-1

* Fri, 13 May 2016 Victor Hu <vhu@us.ibm.com>
  Added ipmitool-xcat-1.8.15-1.ppc64.rpm for RHEL6 ppc64 systems
  Added ipmitool-xcat-1.8.15-1.x86_64.rpm for RHEL6, SLES11 x86_64 systems
  Removed the ipmitool-xcat-1.8.11 for the OS/ARCH replaced by 1.8.15

* Thu, 5 May 2016 Victor Hu <vhu@us.ibm.com>
  Refreshed the xcat-genesis-base:
  - xCAT-genesis-base-x86_64-2.12-snap201605051534.noarch.rpm
  - xCAT-genesis-base-ppc64-2.12-snap201605051443.noarch.rpm

* Tue, 21 Mar 2016 Er Tao Zhao <ertaozh@cn.ibm.com>
  Add perl-http-async_0.30-2_all.deb and perl-net-https-nb_0.14-2_all.deb for
docker lifecycle management

* Wed, 22 Feb 2016 Victor Hu <vhu@us.ibm.com>
  Added ipmitool-xcat-1.8.15-1.ppc64.rpm for RHEL7 ppc64 systems Added
ipmitool-xcat-1.8.15-1.ppc64le.rpm for SLES12 ppc64le systems

* Wed, 18 Nov 2015 Long Cheng <chenglch@cn.ibm.com>
  Add ipmitool-xcat-1.8.15-1.x86_64.rpm which is built on Red Hat Enterprise
  Linux Server release 7.0 to update firmware for OpenPower machine
  Add ipmitool-xcat-1.8.15-1.ppc64le.rpm which is built on Red Hat Enterprise
  Linux Server release 7.1 to update firmware for OpenPower machine

* Tue, 16 Nov 2015 Er Tao Zhao <ertaozh@cn.ibm.com>
  Add xCAT-genesis-base-ppc64-2.11-snap201511160819.noarch.rpm which is build
  on Fedora 23 ppc64. Added modules needed by in-band ipmi operation for
  OpenPOWER servers. Also add DB files for lspci and udevadm.

* Tus, 09 Jun 2015 17:05:29 -0400 Casandra Qiu <cxhong@us.ibm.com>
  Added lldpd packages into xcat-dep for rh7, rh6, sles11.3 and sles12

* Tus, 09 Jun 2015 17:20:29 +0800 Yang Song <yangsbj@cn.ibm.com>
  repackged grub2-xcat-2.02-0.16.el7.snap201506090204.noarch.rpm
  added Homepage info and Licence file

* Thu, 18 May 2015 17:20:29 +0800 Guang Cheng Li <liguangc@cn.ibm.com>
  Added /usr/bin/install into
  xCAT-genesis-base-ppc64-2.10-snap201505102208.noarch.rpm, iprconfig depends on
  /usr/bin/install

* Thu, 15 May 2015 17:27:57 +0800 Guang Cheng Li <liguangc@cn.ibm.com>
  Added iprconfig into xCAT-genesis-base-ppc64-2.10-snap201505102208.noarch.rpm

* Thu, 14 May 2015 04:46:57 +0800 Yang Song <yangsbj@cn.ibm.com>
  - Package grub2-xcat-2.02-0.16.el7.snap201505140423.noarch.rpm has been
    built and repackaged with grub2-2.02-0.16.el7  on ppc64
  - replace grub2-xcat-2.02-0.16.ael7b.build201505100907.noarch.rpm and
    relevant links with grub2-xcat-2.02-0.16.el7.snap201505140423.noarch.rpm

* Sun, 10 May 2015 09:46:57 +0800 Yang Song <yangsbj@cn.ibm.com>
  - Package grub2-xcat-2.02-0.16.ael7b.build201505100907.noarch.rpm has been
    built and repackaged with grub2-xcat-2.02-0.16.ael7b on ppc64
  - replace grub2-xcat-1.0-2.noarch.rpm and relevant links with
    grub2-xcat-2.02-0.16.ael7b.build201505100907.noarch.rpm

* Tue, 28 Apr 2015 16:05:35 +0800 GONG Jie <gongjie@linux.vnet.ibm.com>
  - Package net-snmp-perl has been recompiled for RHEL 7.x on ppc64, ppc64le
    and x86_64.
    + Update the SPEC file with net-snmp-agent-libs >= 1:5.7.2-0 and
      net-snmp-libs >= 1:5.7.2-0. For compatible with RHEL 7.0 and 7.1 and
      later.
  - Remove directory rh7.0

* Wed, 22 Apr 2015 00:00:00 +0800
  - Updated the xCAT-genesis-base-x86_64-2.9-snap201504212134.noarch.rpm
    + Recompiled on CentOS 6.6
    + Added the sg driver for Lenovo servers
